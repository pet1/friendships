json.records do
  json.array! @users do |user|
    json.id user.id
    json.name user.name
    json.friends_status user.status_with(@current_user)
  end
end
