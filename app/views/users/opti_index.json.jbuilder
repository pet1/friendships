json.records do
  json.array! @users do |user|
    p user
    json.id user.id
    json.name user.name
    json.status user.try(:status)
    json.friend_1_id user.try(:friendship_user1)
    json.friend_2_id user.try(:friendship_user2)
    # json.friends_status user.status_with(@current_user)
  end
end
