class ApplicationController < ActionController::API
  include ActionView::Rendering

  def render_to_body(options)
    _render_to_body_with_renderer(options) || super
  end

  unless Rails.env.production?
    around_action :n_plus_one_detection

    def n_plus_one_detection
      Prosopite.scan
      yield
    ensure
      Prosopite.finish
    end
  end
end
