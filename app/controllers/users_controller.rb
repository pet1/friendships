class UsersController < ApplicationController
  def index
    @current_user = User.first
    @users = User.all.includes(:friendships)
  end

  def opti_index
    @current_user = User.first
    @users = User.joins("LEFT JOIN friendships ON users.id = friendships.user_id AND friendships.friend_id = 1 OR users.id = friendships.friend_id AND friendships.user_id = 1").select("users.*, friendships.status, friendships.friend_id as friendship_user2, friendships.user_id AS friendship_user1")
  end
end
