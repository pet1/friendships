class Friendship < ApplicationRecord
  belongs_to :user, inverse_of: :friendships
  belongs_to :friend, class_name: 'User', inverse_of: :friendships

  enum status: { pending: 0, accepted: 1 }
end
