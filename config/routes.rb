Rails.application.routes.draw do
  get '/users', to: 'users#index', defaults: {format: :json}
  get '/users/opti_index', to: 'users#opti_index', defaults: {format: :json}
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
